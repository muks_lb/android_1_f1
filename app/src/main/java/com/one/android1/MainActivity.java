package com.one.android1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.hello);
        tv.setText("Wow!!");
        tv.postDelayed(new Runnable() {
            @Override
            public void run() {
                tv.setText(getResources().getIdentifier("hello", R.string.class.getSimpleName(), getPackageName()));
            }
        }, 2000);
    }
}
